import math
import random
import typing

import topdown

ROWS = 16
COLUMNS = 16


def init_game(rows: int, columns: int) -> dict:
    """
    Initialize a game with the given number of rows and columns in the
    empty state
    """
    return {
        'board': [[False for _ in range(columns)] for _ in range(rows)],
        'ant': {'x': columns // 2, 'y': rows // 2, 'rotation': 0}
    }


def advance_game(game: dict) -> dict:
    """Generate the next step of the game based on the existing game."""
    new_board = [[v for v in row] for row in game['board']]
    x = game['ant']['x']
    y = game['ant']['y']
    rotation = game['ant']['rotation']
    cell = new_board[y][x]
    new_board[y][x] = not cell
    rotation += (int(cell) * 2 - 1) * 90
    y_shift = int(math.cos(2 * math.pi * rotation / 360))
    x_shift = int(-math.sin(2 * math.pi * rotation / 360))
    ant = {
        'x': x + x_shift,
        'y': y + y_shift,
        'rotation': rotation,
    }
    return {'ant': ant, 'board': new_board}


def initialize_animation(
        game: dict
) -> typing.Tuple[topdown.Animation, dict]:
    """Match the animation's initial state to the game's state."""
    rows = len(game['board'])
    columns = len(game['board'][0])
    animation = topdown.Animation(
        x_max=((columns + 1) * 10),
        y_max=((rows + 1) * 10),
        tick_length=0.25
    )
    actors = {
        (r, c): animation.add(
            topdown.Square,
            x=((c + 1) * 10),
            y=((r + 1) * 10),
            size=8,
            color='black' if value else 'white',
        )
        for r, row in enumerate(game['board'])
        for c, value in enumerate(row)
    }
    actors['ant'] = animation.add(
        topdown.Triangle,
        x=(game['ant']['x'] + 1) * 10,
        y=(game['ant']['y'] + 1) * 10,
        rotation=game['ant']['rotation'],
        color='green',
        size=8,
    )
    return animation, actors


def sync_actors(game: dict, actors: dict):
    """Syncronize the state of the actors with the state of the game."""
    for r, row in enumerate(game['board']):
        for c, value in enumerate(row):
            actor = actors[(r, c)]
            color = 'black' if value else 'white'
            actor.set_color(color)
    ant = game['ant']
    actors['ant'].move_to((ant['x'] + 1) * 10, (ant['y'] + 1) * 10)
    actors['ant'].rotate_to(ant['rotation'])


def main():
    """Play langton's ants."""
    game = init_game(ROWS, COLUMNS)
    animation, actors = initialize_animation(game)
    for _ in range(300):
        animation.tick()
        animation.tick()
        game = advance_game(game)
        sync_actors(game, actors)
    animation.render_to('./langtons_ants.svg')

if __name__ == '__main__':
    main()
