import random
import typing

import topdown

ROWS = 35
COLUMNS = 60

Board = typing.List[typing.List[bool]]


def random_board(rows: int, columns: int) -> Board:
    """
    Initialize a board with the given number of rows and columns in a random
    state.
    """
    board = []
    for _ in range(rows):
        board.append([bool(random.randint(0, 1)) for _ in range(columns)])
    return board


def count_neighbors(
        board: Board,
        row: int,
        column: int,
) -> int:
    """
    Count the number of neighbors the specified cell has.

    :param board:
        The board of life holding cells that are either alive (True)
        or dead (False).
    :param row:
        The row number of the cell being evaluated.
    :param column:
        The column number of the cell being evaluated.
    :return:
        The cell's number of living neighbors.
    """
    rows = len(board)
    columns = len(board[row])
    top = (row + 1 + rows) % rows
    bottom = (row - 1 + rows) % rows
    left = (column - 1 + columns) % columns
    right = (column + 1 + columns) % columns
    return sum([
        board[top][left],
        board[top][column],
        board[top][right],
        board[row][left],
        board[row][right],
        board[bottom][left],
        board[bottom][column],
        board[bottom][right],
    ])


def next_cell_status(current: bool, neighbors: int) -> bool:
    """
    Determine if the cell should be alive in the next turn.

    :param current:
        Whether the cell is currently alive.
    :param neighbors:
        The number of neighbors the cell has.
    :return:
        Whether the cell should be alive on the next turn.
    """
    if current and neighbors == 2:
        return True
    return neighbors == 3


def advance_board(board: Board) -> Board:
    """Generate the next board based on the existing board."""
    return [
        [
            next_cell_status(value, count_neighbors(board, r, c))
            for c, value in enumerate(row)
        ]
        for r, row in enumerate(board)
    ]


def initialize_animation(
        board: Board
) -> typing.Tuple[topdown.Animation, dict]:
    """Match the animation's initial state to the board's state."""
    rows = len(board)
    columns = len(board[0])
    animation = topdown.Animation(
        x_max=((columns + 1) * 10),
        y_max=((rows + 1) * 10)
    )
    actors = {
        (r, c): animation.add(
            topdown.Circle,
            x=((c + 1) * 10),
            y=((r + 1) * 10),
            size=8,
            visible=value,
        )
        for r, row in enumerate(board)
        for c, value in enumerate(row)
    }
    return animation, actors


def sync_actors(board: Board, actors: dict):
    """Syncronize the state of the actors with the state of the board."""
    for r, row in enumerate(board):
        for c, value in enumerate(row):
            actor = actors[(r, c)]
            actor.show() if value else actor.hide()


def main():
    """Play game of life."""
    board = random_board(ROWS, COLUMNS)
    animation, actors = initialize_animation(board)
    for _ in range(180):
        animation.tick()
        animation.tick()
        board = advance_board(board)
        sync_actors(board, actors)
    animation.render_to('./game_of_life.svg')

if __name__ == '__main__':
    main()
