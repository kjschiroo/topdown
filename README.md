# TopDown

A library for creating 2D animations of simulations.

## Install

```
pip3 install topdown
```

## Usage

Create two circles and move them across the screen.

```python
import topdown

animation = topdown.Animation(x_max=100, y_max=100)
top_circle = animation.add(topdown.Circle, x=75, y=75)
bottom_circle = animation.add(topdown.Circle, x=25, y=25)

animation.tick()  # Advance to the next frame in the animation
top_circle.move_to(x=25, y=75)
bottom_circle.move_to(x=75, y=25)

animation.render_to('example.svg')
```

Move a circle along an arbitrary path without manually ticking through frames.

```python
animation = topdown.Animation(x_max=100, y_max=100)
circle = animation.add(topdown.Circle, x=0, y=0)
for t in animation.frames():
    y = t * t
    circle.move_to(x=t, y=y)
    if y > animation.y_max:
        # The loop will run indefinitely until we break out of it
        break
animation.render_to()
```

## Examples

### Conway's Game of Life

```
examples/game_of_life.py
```

![Image of game of life](animations/game_of_life.svg)

### Langton's Ants

```
examples/langtons_ants.py
```

![Image of langton's ants](animations/langtons_ants.svg)
